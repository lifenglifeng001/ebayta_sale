#!/usr/bin/env python
# -*-coding: utf-8 -*-

from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp import _

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    credit_check = fields.Boolean('Credit check', default=False)
    delivery_method = fields.Selection([
        ('express', 'Express'), ('self', 'Self'), ('city', 'City'), ('logistics', 'Logistics')],
        'Delivery method')
    payment = fields.Boolean('Payment')
    stock = fields.Boolean('Stock')
    partner_credit_line = fields.Float(related='partner_id.credit_line')

    @api.multi
    def action_check_credit(self):
        self.ensure_one()
        credit_line = self.partner_id.credit_line
        current_order_amount = self.amount_total
        receivable = self.partner_id.credit

        if credit_line < receivable + current_order_amount:
            raise Warning(_('Exceed credit line'))
        else:
            self.credit_check = True

    @api.multi
    def action_special_confirm(self):
        super(SaleOrder, self).action_confirm()

    @api.multi
    def action_confirm(self):

        if not self.stock:
            raise Warning(_('Stock was not enough'), )
        if self.partner_credit_line and not self.credit_check:
            raise Warning(_('Exceed credit line'), )
        if not self.payment:
            raise Warning(_('Not payment'))

        super(SaleOrder, self).action_confirm()

    @api.multi
    def action_check_stock(self):
        """ check product stock"""
        self.ensure_one()
        not_enough = {}
        for line in self.order_line:
            product_id = line.product_id
            if line.product_uom_qty > product_id.qty_available:
                not_enough[product_id.id] = product_id.name

        if not_enough:
            product_str = ', '.join(not_enough.values())
            warn = _("%s stock was not enough") % product_str
            raise Warning((_('Stock warning'), warn))
        else:
            self.stock = True
