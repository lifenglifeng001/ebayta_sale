#!/usr/bin/env python
# -*-coding: utf-8 -*-

from openerp import models, fields

class res_partner(models.Model):
    _inherit = 'res.partner'

    credit_line = fields.Float('Credit line')
